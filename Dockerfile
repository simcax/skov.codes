FROM cgr.dev/chainguard/nginx:latest

COPY public /usr/share/nginx/html

WORKDIR /usr/share/nginx/html
