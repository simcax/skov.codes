---
title: "Kubernetes on Raspberry Pi"
date: 2021-10-02T06:22:06+01:00
draft: false
author: "Carsten"
featured_image: "images/raspberrypi4Cluster_small_w_logos.png"
---
# Kubernetes
I've been running a k8s cluster on Raspberry Pi 4s for about 2-3 months at the time of this article.

It's been a very good learning experience, one I'd recommend anyone wanting to learn how to administrate Kubernetes or getting more into cloud and containers.
I mainly went this way, because I have some Python applications, which I was looking to get running in containers. On top of that Cloud is all the buzz at work these days.

The cluster currently consists of:
- 3 [Raspberry Pi 4b](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/) 8GB (nodes)
- 1 [Raspberry Pi 4b](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/) 4GB (master node)

So the box on the left (simpi400) is my master node, and contains:

[Geekworm SATA HAT](https://wiki.geekworm.com/index.php/X825)
[Geekworm Case](https://wiki.geekworm.com/index.php/X825-C2_Case)
Kingston 240gb SSD

I am still undecided if it was a good idea to use the Pi, with least RAM as my Master Node. I still have some work to do in the observability space for this cluster. This will come in a later article.

The two Pi's on the righthand side are my data nodes, and are made of:

- [Argon ONE M.2 Case](https://www.argon40.com/argon-one-m-2-case-for-raspberry-pi-4.html)
- [Verbatim 1TB M.2 SSD](https://www.verbatim-europe.co.uk/en/prod/vi560-s3-m2-ssd-49364/)

The final Pi in the 1 stack tower, is my application node, for applications which needs no storage.

# Github

I have started a repo for automating the setup of my Raspberry Pi Cluster. It has a lot of shortcommings so far, and is not entirely working as I hoped it would. But it's a learning experience for me, and I hope to complete it during the coming time.
The repo can be found here: https://github.com/simcax/deploy-k8s

I am especially not happy about the node adding part. It eluded me in the beginning, that you needed to get a seperate token for each node, and so I added them manually while learning.
Another things is that it should be made into an Ansible Role, so that will come in future iterations.
For now it might serve as inspiration.

# What's running on it?

Right now:

- this site is running on my cluster
- A Python webapp in the works
- A Cockroach DB Cluster

More will come as I get my different projects migrated to container tech :-) More about that in a coming article.

