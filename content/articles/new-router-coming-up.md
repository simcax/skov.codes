---
title: "New Router Coming Up"
date: 2022-04-07T07:10:12+02:00
draft: false
featured_image: "images/router/router-apu4.jpg"
---
# Finally a new router!
My internet connection has been pretty annoying for the past couple of years - since I started my homelab up again. There has been multiple issues, the most annoying of them not being able to serve a VPN based on wireguard. 

The firewall setup on my old connection, paid by my previous employer, was so restrictive, that it did not work.

For the first time in many years, I have taken over my internet connection and pay for it myself. This also entails, making choices I couldn't before. 

I have now changed my connection to Hiper, and am able to use my own router. After trying to get my Ubiquity Security Gateway (USG) to work, I have decided to use [pfsense](https://www.pfsense.org/) and have my router be running on Open Source software, just like my servers and laptops. 

By chance I stumbled upon [TekLager](https://teklager.se/en/) - a Swedish webshop, driven by a couple of cool Open Source evangalists as a side project to their full-time jobs. They sell hardware to run open source router software on - with really great specs. And in my case I found a router, which was quite a bit cheaper than the official Netgate offering. Especially since I now have to support full one GBit speed, and would like to harness that speed on my VPN as well. Something which would have set my back almost 200$ more than the [APU4D4](https://teklager.se/en/products/routers/apu4d4-open-source-router) I ended up with from TekLager. 

The specs looks really promising, and I don't see why the router shouldn't live up to them. 

I can't wait to get started configuring the router, but it will have to wait for a couple of days, since I am right now traveling, and won't be back till Monday. In addition the days up to Easter will be busy, so probably won't be doing much till the Easter hits, and there will be some days with little else to do ;-)

I will post a new article, once I had some time working on the new router - hopefully having it configured and up and running.