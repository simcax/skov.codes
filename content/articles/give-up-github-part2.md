---
title: "Give Up Github Part 2"
date: 2022-08-27T22:30:46+02:00
draft: false
featured_image: /images/leave-github-part2/hugo.logo.svg.png
---
# It doesn't have to be hard
As part of my project to leave Github - as I wrote about in [part 1](https://www.skov.codes/articles/give-up-github-part1/) - I wanted to try out moving the CI part of building the [skov.run](https://www.skov.run) site. A site built with HUGO and running in a container on my Kubernetes cluster. 

# But you can make it hard :-)
So I have moved a number of repositories, most of them with Helm Charts, and one with a Python application. So when I started this migration, I thought it would be piece of cake. 

Building the HUGO site and then the container, proved to be somewhat of a struggle. But only because I was overthinking it. I thought I had to install all sorts of things in the container, log in to docker hub and push the image.

In the end, it turned out to be REALLY SIMPLE!

# How it is done
[Codeberg's CI](https://ci.codeberg.org) runs [Woodpecker](https://woodpecker-ci.org/). Woodpecker looks after a CI file or pipeline file if you will, which by default is `.woodpecker.yml` - and resides in the root of the repo. 

The pipeline can use [Drone plugins](http://plugins.drone.io/) - although Woodpecker has a slightly different YAML structure. 

The pipeline ended as this simple YAML:
```yaml
pipeline:
  hugo:
    image: plugins/hugo
  build:
    image: plugins/docker
    settings:
      username: simcax
      password: 
        from_secret: CONTAINER_PASSWORD
      repo: simcax/skov-run
      tags: ${CI_BUILD_NUMBER}
```

The first part "hugo" builds the HUGO site with the hugo plugin. And the second part "build", uses the Docker plugin to build and push the docker image. 

The old pipeline on Github Actions was a bit longer:
```yaml
name: Build image
on: push
jobs:
  build:
    runs-on: ubuntu-latest
    steps:
      - name: Git checkout
        uses: actions/checkout@v2

      - name: Update theme
        run: git submodule update --init --recursive

      - name: Setup hugo
        uses: peaceiris/actions-hugo@v2
        with:
          hugo-version: "0.94.1"

      - name: Build
        run: hugo --minify
      
      - name: List
        run: ls -l public

      -
        name: Set up QEMU
        uses: docker/setup-qemu-action@v1
      -
        name: Set up Docker Buildx
        uses: docker/setup-buildx-action@v1
      -
        name: Login to DockerHub
        uses: docker/login-action@v1 
        with:
          username: ${{ secrets.DOCKERHUB_USERNAME }}
          password: ${{ secrets.DOCKERHUB_TOKEN }}
      -
        name: Build and push
        id: docker_build
        uses: docker/build-push-action@v2
        with:
          push: true
          context: .
          tags: simcax/skov.codes:${{ secrets.MAJOR_VERSION }}.${{ secrets.FEATURE_VERSION }}.${{ github.run_number}}
    
```

After getting the image built successfully, it was a matter of migrating the repo containing the Helm chart, and pointing Argo CD to the new Repo.

So the application YAML ended this way:

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: skov-run
  namespace: argo-namespace
  annotations:
    argocd-image-updater.argoproj.io/image-list: skov-run=simcax/skov-run
    argocd-image-updater.argoproj.io/skov-run.pull-secret: pullsecret:argo-namespace/secret-containing-registry-secret
    argocd-image-updater.argoproj.io/write-back-method: git:secret:argo-namespace/secret-containing-repo-secret
    argocd-image-updater.argoproj.io/git-branch: main
spec:
  project: default

  source: 
    repoURL: https://codeberg.org/simcax/deploy-skov.run.git
    targetRevision: HEAD
    path: skov-run
  destination:
    server: https://kubernetes.default.svc
    namespace: the-skov-run-namespace
  syncPolicy:
    syncOptions:
    - "CreateNamespace=true"
    automated:
      selfHeal: true
      prune: true

```

To make things more dynamic, I added the image updater annotations, so whenever a new image is pushed to docker hub, it will be deployed. 
Making it really easy to publish blog posts. Just a matter of adding the markdown and checking it in to the repo. From there it's an automatic process.

# Current status
So far I have migrated about 9 of 44 repositories from Github to [Codeberg](https://codeberg.org), coming close to 25%.
It is not going as fast as I thought it would, but now I have the basics down I believe.

I hope to get the migration done during the next month or two.


