---
title: "Give Up Github Part 1 - is it hard?"
date: 2022-07-28T17:45:49+02:00
draft: false
featured_image: "images/leave-gihub/yancy-min-842ofHC6MaI-unsplash.jpg"
---
# TL;DR
Sometimes when things seem too good to be true, they are. 
Real short, in building co-pilot Microsoft utilize open source, monitize it and don't contribute back to the community.

In the article here I investigate how hard it is to leave Github, using the suggested alternatives. 

## BUT Github is nice!
...so why would anyone want to leave? Well, as it turns out, it seems Microsoft has done it again. 

Taken up an eco system or product, and closed things up. 
I first got wind of things in [this tweet](https://twitter.com/conservancy/status/1542503499094626304) from "Software Freedom Conservancy" [@conservancy](https://twitter.com/conservancy)

It all revolves around co-pilot - yeah that really nice piece of software helping all us lazy developers writing faster code.
The thing is - co-pilot is trained on all the open source software repos on Github, and then Github monitizes that.
There is no free tier i.e. for the open source community it's built on top of. The training is only done on open source, and not even Microsofts own software like Office or Windows. I recently had a discussion with a seasoned C programmer, hinting that it might be because the code quality is simply better for Open Source software - I will let you contemplate that for yourself.

At the same time Microsoft has not answered questions put forward by the Software Freedom Conservancy, which to me seems to indicate bad will,
 or at least not showing the openness Microsoft is so keen on, when being supportive of open source elsewhere.

> Read the [blog post](https://sfconservancy.org/blog/2022/jun/30/give-up-github-launch/) by Software Freedom Conservancy and make up your own opinion on the matter if Github is to be left. 
Being a strong believer in FOSS, I no longer have any doubt, it has to be done.

I think it all makes a rather compelling case, and thus I ventured to move one of my repositories. 

# My process - the alternatives
Software Freedom Conservancy points to some alternatives, and 2 of those are hosted:

+ [Codeberg](https://www.codeberg.org)
+ [Sourcehut](https://sr.ht)

After trying out both, I guess there is no doubt Codeberg has the most familiar look and feel, when coming from Github. 
I however tried out Sourcehut as the first alternative, since there is a build service readily available, and I make use of Github Actions. The CI on Codeberg is currently still being built out, although it is fully possible to gain access by request.

## Sourcehut
The feel is very much back to the beginning of the internet, when you first visit Sourcehut. The design is raw, and basic. I quite like it though.

![sourcehut](/images/leave-gihub/sourcehut.png "Sourcehut is nice and simple in it's design")*Sourcehut repo page for one of my projects*

No extra things to distract you from the thing you are here to do - build great software and get things done. 

The process of creating a new repository is easy and straight forward, and not the part I was mostly concerned about when doing the switch. I have come to rely on the simplicity and power of Github Actions.

As I do development on my own, and 99% of the time do trunk based development without a PR flow, it had not dawned on me, that sourcehut makes use of a different PR process. So if you have the need for a PR flow, I suggest you read [this good blog post](https://postmarketos.org/blog/2022/07/25/considering-sourcehut/) from [postmarketOS](https://postmarketos.org/). Here they discuss their thoughts on moving to sourcehut. The drawback as I understand it, is not having the PR flow many has come to know and feel familiar with, but instead having the old style git-email flow of patching. Something I have never had any experience with myself.

My focus on moving away from Github, is mainly one of having a repo, and a build service. Making it possible to test and build my personal projects, ending up pushing the container images to an image registry.
### Building and testing your software
The build service is quite basic, as far as I have been able to gather. You get a fresh VM, and can run any shell commands to test and build your software.

It is still a YAML file as with Github Actions, but a bit more basic, and without the actions I have come to rely heavily on.
That being said, it took me a couple of nights to port my projects Github Actions pipelines to Sourcehut's build pipeline. 

On the left is Sourcehut's .build.yaml and on the right the origin Github Actions pipeline. 

![gh-actions-vs-sr-ht-build](/images/leave-gihub/gh-actions-vs-sr-ht-build.png)*Github Actions YAML vs Sourcehut build YAML*

At first glance the sourcehut build YAML file is longer than the corresponding Github Actions file. What might not be apparent from the screenshort, is the added explicitness needed on sourcehut. 

First thing I came upon is how secrets needs to be first defined for use in the pipeline:

![sr-ht-secrets](/images/leave-gihub/sr-ht-secrets.png)


... then set by reading the secret from a file

![sr-ht-secrets-from-file](/images/leave-gihub/sr-ht-secrets-from-file.png)


... and then exporting the variable thus exposing it to the environment.

![sr-ht-secrets-export](/images/leave-gihub/sr-ht-secrets-export.png)

 This is something which just works smoothly on Github in one take. 

 ![github-secrets](/images/leave-gihub/gh-secrets.png)
 *Github equivalent in one simple command*
 
 So a bit of a pain, but manageable.

Another pain on using the sourcehut build service, is the reset being done between tasks. The environment variables are not carried from one step to another. And this is quite cumbersome, having to set up things like the path to include the install path to i.e. pytest or flake8 on each task. 

I like to split my CI into:

+ linting
+ testing
+ building

For each of these phases, it is then needed to do a sort of initiliazing of the environment. I can see some places that might be a good thing, I just wish it could have been a setting - if it's there I at least didn't find it yet.

Otherwise the jist of it, is being provided with a Virtual Machine, which you can do whatever you want with - more or less. You can install needed packages and execute needed programs to run your CI. 

# Codeberg
Using Codeberg howerver is in my opinion the nicer of the two for sure. It is a lot more like Github, and I can see this as being the new home for Open Source projects. 

![codeberg-repo](/images/leave-gihub/codeberg-repo.png)
*Codeberg examples for CI - https://codeberg.org/Codeberg-CI/examples*

This is the way I will be moving, and I recently gained access to Codeberg CI and have migrated one build already. Codeberg CI is built on [Woodpecker](https://woodpecker-ci.org/), which looks to be a really nice CI engine, Open Source and free, and running containers for each step. 

## Codeberg CI

Last night I tinkered around moving my first Github Actions pipeline to the Woodpecker YAML format and running it on Codeberg CI. It was easy and intuitive when coming from Github Actions. The formats are quite similar, and I even got a matrix build up for running both Python 3.10 and 3.11. 

The [documentation](https://woodpecker-ci.org/docs/intro) has a good *getting started* section, and is generally easy to follow and understand. The interface for Woodpecker is absolutely great. 

![woodpecker-ci](/images/leave-gihub/codeberg-ci.png)

The list of jobs is also quite nice - and who can't use a little woodpecker in their life, while waiting for a build :-D 

![woodpecker-ci-jobs](/images/leave-gihub/codeberg-ci-jobs.png)

To compare the YAML of Codeberg to Github, and relate it to the sourcehut experience, here is a side by side comparison of Codeberg/Woodpecker YAML on the left, and Github YAML on the right. 

![codeberg-ci-yaml-vs-github](/images/leave-gihub/codeberg-ci-yaml-vs-gh.png)

The woodpecker YAML is actually shorter, which was a bit of a surprise. The secret and environment variable definition is a lot more similar to the Github Actions style of doing it. 

```yaml
environment:
      - API_BASE_URL=https://foreninglet.dk/api/
      - API_VERSION=version=1
      - API_MEMBERS_API=members
```

```yaml
secrets: [ API_USERNAME, API_PASSWORD, MAILCHIMP_KEY, MAILCHIMP_USERNAME, MAILCHIMP_AUDIENCE_ID, MAILCHIMP_SERVER ]
```

I have only briefly scratched the surface of this, but it took me no time at all migrating from Github Actions to Woodpecker/Codeberg CI. In stark contrast to the more cumbersome work of getting sourcehut's build service to do the same. No doubt in my mind, that migrating to Woodpecker from Github Actions will be a small part of migrating. Something I was not looking forward to when considering sourcehut. 

Next project I will be migrating needs a couple of services to run the tests - Redis and a Cockroach DB - and it will be interesting to try. The spec for setting up services looks quite straight forward. So I am looking forward to trying that out. 

## Migrations
There is also a function of migrating your existing repositories from other Git providers to Codeberg. An easy and painless process of pointing to your repo, giving a few details, and the repo is smoothly migrated to Codeberg! 

I have currently migrated 3 repositories this way, and it was fast without issues. 
# Other solutions
I also looked a bit into Gitlab, and that seems to be a good alternative as well. There is a free tier, and both the SCM and CI part looks wholesome. However I would like to stay with something entirely Open Sourced, and look forward to be able to pay for and support Codeberg. 

# Conclusion for now...
There are alternatives to Github, and it is mostly work and elbow grease needed to get things running. Currently my bet is on Codeberg, I love the clean UI and Woodpecker is absolutely amazing. 

There are a lot of facets of running open source projects on Github, that I have no experience with. So things like PR flows, issues a.s.o. is something needing to be looked into, if you use those features.

Another thing is the added services, which Github has - things like integration to a myriad of services, Dependabot and others. If these pose a big part of the daily motions, there might be harder times ahead I gather.

I will be trying out working with Codeberg for a few of my projects for a while. Once I see things are good, I will do a full scale migration of my repositories. 

Hoping to do a new post on the migration in a month or two. So be sure to keep an eye on my [twitter](https://twitter.com/simcax) for that.




