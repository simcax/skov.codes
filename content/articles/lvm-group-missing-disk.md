---
title: "LVM Group Missing Disk"
date: 2023-12-03T13:09:44+01:00
draft: true
author: "Carsten"
featured_image: "images/lmv-group-missing-disk/lvm-device-missing.png"
---
# A disk went missing
I recently had to shutdown my proxmox server, as all the power in my appartment needed to be turned off. 
And as things go, I of course had a disk missing when I booted it again. Unfortunately the disk was part of a volume group (LVM), and to make things even worse, it was the LVM holding my Kubernetes storage. 

## So just remove it...
... was my first thought. And then read in the backup from last night. This is where a many hour troubleshooting session started. 

What I wanted was simply to remove the whole LVM, and start over. This proved to be difficuly, since any try at that failed with the error: 

```shell
 `Failed to update pool vgdatagrp1/ssd1`
```