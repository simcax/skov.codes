---
title: "A Python Developer Gone Pi"
date: 2022-03-16T19:25:45+01:00
draft: false
featured_image: "images/raspberrypi4Cluster_small_w_logos.png"
---
Articles about Kubernetes and TDD Development - some of it using Raspberry Pi.
{{< rawhtml >}}
  <p class="speshal-fancy-custom">
    <a href="https://social.linux.pizza/@Simcax" rel="me">I'm on Mastodon!</a>
  </p>
{{< /rawhtml >}}
[](/static/images/mastodon.svg)
